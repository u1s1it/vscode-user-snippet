### 1.使用方法
---

#### 下载

进入 VSCode 的用户代码片段配置文件目录, 执行下面命令:

```
git clone https://gitee.com/u1s1it/vscode-user-snippet.git ../snippets
```

#### Mac

用户代码片段配置文件存放目录示例:

```
/Users/liang/Library/Application Support/Code/User/snippets
```

#### Windows

用户代码片段配置文件存放目录示例:

```
C:\Users\Administrator\AppData\Roaming\Code\User\snippets
```

### 2.配置文件
---

#### Markdown

在 `settings.json` 添加配置项

```json
"[markdown]":{
    // 关闭保存自动格式化
    "editor.formatOnSave": false,
    // markdown 文件开启代码片段提示
    "editor.quickSuggestions": true,
}
```

### 3.代码片段

#### Markdown

| 触发  | 描述   |
| :-----:  | :-----:  |
| a     | 生成 markdown 链接 |
| img   | 生成 markdown 图片 |
| table   | 生成 markdown 表格 |
| `html   | 生成代码块,支持语言: html，css，js，php，json，shell |

#### JavaScript

| 触发  | 描述   |
| :-----: | :-----:  |
| edo | 默认导出一个对象 |

#### 全局代码片段

适用所有语言

| 触发  | 描述   |
| :-----:  | :-----:  |
| rr     | 快速生成 return |

适用语言: `javascript,typescript`

| 触发  | 描述   |
| :-----:  | :-----:  |
|    la  | 快速定义 let 变量 |
|    ca  | 快速定义 const 变量 |